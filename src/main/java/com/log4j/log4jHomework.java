package com.log4j;

import com.log4j.exception.CustomException;
import org.apache.logging.log4j.*;
import com.log4j.services.RandomGenerator;
import java.util.Random;


public class log4jHomework {

    private static Logger demonologger = LogManager.getLogger(log4jHomework.class);
    private static final String MSG_SUCCESS = "Приложение успешно запущено";

    public log4jHomework() {
    }

    public static void main(String[] args) {
        Random rnd = new Random();
        RandomGenerator rndGen = new RandomGenerator(rnd);
        generate(rndGen, 30);
    }

    public static void generate(RandomGenerator rnd, int count) {
        for (int i = 0; i < count; i++) {
            try {
                rnd.randomNum();
                demonologger.info(MSG_SUCCESS);
            } catch (CustomException e) {
                demonologger.info(e.getMessage());
            }
        }
    }
}
