package com.log4j.services;

import com.log4j.exception.CustomException;
import java.util.Random;

public class RandomGenerator {

    private Random random;

    public RandomGenerator(Random random) {
        this.random = random;
    }

    public int randomNum() throws CustomException {
        int num = random.nextInt(10 - 0 + 1);
        if (num <= 5) {
            throw new CustomException(String.valueOf(num));
        }
        return num;
    }
}
