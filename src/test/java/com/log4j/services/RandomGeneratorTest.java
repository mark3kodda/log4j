package com.log4j.services;

import com.log4j.exception.CustomException;
import org.mockito.Mockito;
import java.util.Random;
import static org.junit.jupiter.api.Assertions.*;

class RandomGeneratorTest {

    private final Random rnd = Mockito.mock(Random.class);
    private final RandomGenerator cut = new RandomGenerator(rnd);

    @org.junit.jupiter.api.Test
    void randomNumLesserThan5Test() {
        int pickedNumber = 1;
        Mockito.when(rnd.nextInt(11)).thenReturn(pickedNumber);

        assertThrowsExactly(CustomException.class, cut::randomNum,
                " Сгенерированное число - " + pickedNumber);
    }

    @org.junit.jupiter.api.Test
    void randomNumGreaterThan5Test() throws CustomException {
        int expected = 9;
        Mockito.when(rnd.nextInt(11)).thenReturn(expected);

        int actual = cut.randomNum();

        System.out.println(actual);

        assertEquals(expected, actual);
    }

}